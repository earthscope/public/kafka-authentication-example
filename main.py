import os
import aiokafka
import aiokafka.helpers
from typing import Union

# As accessed at https://www.amazontrust.com/repository/AmazonRootCA1.pem
# Hard-coding here as this CA cert is valid until 2038
AMAZON_ROOT_CA_1_PEM = """-----BEGIN CERTIFICATE-----
MIIDQTCCAimgAwIBAgITBmyfz5m/jAo54vB4ikPmljZbyjANBgkqhkiG9w0BAQsF
ADA5MQswCQYDVQQGEwJVUzEPMA0GA1UEChMGQW1hem9uMRkwFwYDVQQDExBBbWF6
b24gUm9vdCBDQSAxMB4XDTE1MDUyNjAwMDAwMFoXDTM4MDExNzAwMDAwMFowOTEL
MAkGA1UEBhMCVVMxDzANBgNVBAoTBkFtYXpvbjEZMBcGA1UEAxMQQW1hem9uIFJv
b3QgQ0EgMTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALJ4gHHKeNXj
ca9HgFB0fW7Y14h29Jlo91ghYPl0hAEvrAIthtOgQ3pOsqTQNroBvo3bSMgHFzZM
9O6II8c+6zf1tRn4SWiw3te5djgdYZ6k/oI2peVKVuRF4fn9tBb6dNqcmzU5L/qw
IFAGbHrQgLKm+a/sRxmPUDgH3KKHOVj4utWp+UhnMJbulHheb4mjUcAwhmahRWa6
VOujw5H5SNz/0egwLX0tdHA114gk957EWW67c4cX8jJGKLhD+rcdqsq08p8kDi1L
93FcXmn/6pUCyziKrlA4b9v7LWIbxcceVOF34GfID5yHI9Y/QCB/IIDEgEw+OyQm
jgSubJrIqg0CAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMC
AYYwHQYDVR0OBBYEFIQYzIU07LwMlJQuCFmcx7IQTgoIMA0GCSqGSIb3DQEBCwUA
A4IBAQCY8jdaQZChGsV2USggNiMOruYou6r4lK5IpDB/G/wkjUu0yKGX9rbxenDI
U5PMCCjjmCXPI6T53iHTfIUJrU6adTrCC2qJeHZERxhlbI1Bjjt/msv0tadQ1wUs
N+gDS63pYaACbvXy8MWy7Vu33PqUXHeeE6V/Uq2V8viTO96LXFvKWlJbYK8U90vv
o/ufQJVtMVT8QtPHRh8jrdkPSHCa2XV4cdFyQzR1bldZwgJcJmApzyMZFo6IQ6XU
5MsI+yMRQ+hDKXJioaldXgjUkK642M4UwtBV8ob2xJNDd2ZhwLnoQdeXeGADbkpy
rqXRfboQnoZsG4q5WTP468SQvvG5
-----END CERTIFICATE-----
"""

def bytes_to_str(v: bytes):
    """Deserializes a `bytes` object to `str` using UTF-8"""
    return str(v, "utf-8")


async def main(
    bootstrap_servers: str,
    certfile: os.PathLike,
    keyfile: os.PathLike,
    password: str,
    group_id: Union[str, None] = None,
):
    consumer = aiokafka.AIOKafkaConsumer(
        # The topic to consume from
        "public.gnss.positions.normalized.geojson",
        
        # The Kafka brokers
        bootstrap_servers=bootstrap_servers,

        # optional group ID if part of a consumer group
        group_id=group_id,
        
        # Key & Value for this topic are strings
        key_deserializer=bytes_to_str,
        value_deserializer=bytes_to_str,
        
        # Start from the end of the topic. Use `earliest` to start at the beginning of the topic
        auto_offset_reset="latest",
        
        # Create an SSL context
        security_protocol="SSL",
        ssl_context=aiokafka.helpers.create_ssl_context(
            cadata=AMAZON_ROOT_CA_1_PEM,

            # Signed certificate chain (with root CA at the top of the chain)
            certfile=certfile,

            # Private Key file of `certfile` certificate
            keyfile=keyfile,

            # Password to the encrypted private key in `keyfile`
            password=password,
        ),
    )

    # Start the consumer
    await consumer.start()

    # Consumer implements python iterator. Can also get using methods: `await consumer.getone()` or `await consumer.getmany()`
    record: aiokafka.ConsumerRecord[str, str]
    async for record in consumer:
        print(f"Key: {record.key} | Value: {record.value}")


if __name__ == "__main__":
    import asyncio
    import os

    # Get config from env
    try:
        BOOTSTRAP_SERVERS = os.environ["BOOTSTRAP_SERVERS"]
        SSL_PASSWORD = os.environ["SSL_PASSWORD"]
        SSL_CLIENT_CERT = os.environ["SSL_CLIENT_CERT"]
        SSL_CLIENT_CERT_KEY = os.environ["SSL_CLIENT_CERT_KEY"]
        GROUP_ID = os.environ.get("GROUP_ID", None)
    except KeyError as e:
        print(f"Missing required env variable: {e}")
        exit(1)

    asyncio.run(
        main(
            bootstrap_servers=BOOTSTRAP_SERVERS,
            group_id=GROUP_ID,
            certfile=SSL_CLIENT_CERT,
            keyfile=SSL_CLIENT_CERT_KEY,
            password=SSL_PASSWORD,
        )
    )
