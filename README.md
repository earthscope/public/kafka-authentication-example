# kafka-authentication-example

An example application using AIOKafka to connect to our public Kafka cluster.

## Getting started

Note: this example was developed against Python 3.9

0. Install the [Snappy compression library from Google](https://github.com/google/snappy).

   - APT: sudo apt-get install libsnappy-dev
   - RPM: sudo yum install snappy-devel
   - Brew: brew install snappy

1. Set up and activate a python virtual environment

   ```shell
   python3 -m venv venv
   . venv/bin/activate
   ```

2. Install dependencies

   ```shell
   pip install -r requirements.txt
   ```

3. Configure your environment variables

   ```shell
   export BOOTSTRAP_SERVERS=""
   export SSL_PASSWORD=replace_me_with_the_real_password
   export SSL_CLIENT_CERT=/path/to/client_certificate.pem
   export SSL_CLIENT_CERT_KEY=/path/to/client_certificate.key

   # Optional
   export GROUP_ID=my-consumer-group-id
   ```

   For an Earthscope authentication bundle:
   SSL_PASSWORD will be the ssl.key.password within (CN).relative.client.props.
   SSL_CLIENT_CERT will be the (CN).certificate.pem
   SSL_CLIENT_CERT_KEY will be (CN).certificate.key

4. Run the program

   ```shell
   python main.py
   ```
